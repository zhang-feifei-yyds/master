#### 项目介绍
微服务项目架构 实现支持高并发、高可用的微服务项目

#### 项目规划
目前已实现的功能为下列列举项,该项目正在一步步开发完善中...

#### 项目技术方案

1.  Sentinel：把流量作为切入点，从流量控制、熔断降级、系统负载保护等多个维度保护服务的稳定性。(持久化到nacos以及与网关API进行了整合),服务调用负载均衡：Ribbon、OpenFeign;服务熔断，隔离，与降级。采用配置持久化到nacs实现系统流控
2.  Nacos：（支持集群部署）一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台。注册中心和配置中心(系统采用多环境配置)
3.  Gateway：服务网关 （支持集群部署）
4.  Seata：高性能微服务分布式事务解决 （项目采用AT模式支持集群部署以及注册进nacos和事务协调者服务信息持久化到mysql）
3.  RocketMQ：基于高可用分布式集群技术，提供低延时的、高可靠的消息发布与订阅服务
3.  Gateway：服务网关 （支持集群部署）
4.  Nginx：负载均衡服务器 （负载均衡Gateway网关）
5.  Swagger: 单服务单独配置，网关服务对swagger进行了根据Gateway的断言规则进行了自动聚合
6.  Oauth2.0+Jwt+Security+Redis: 实现单独鉴权服务。网关进行拦截校验。
7.  xxl-job: 实现分布式定时任务调度。
8.  OSS: 阿里云文件对象存储。

#### 项目模块介绍

1.  ming-authorization 鉴权服务
2.  ming-common  公共模块
3.  ming-config  配置模块(后期这里面放一些自己的start)
4.  ming-core    核心代码模块
5.  ming-gateway 网关模块
6.  ming-job     定时器模块
7.   |   -ming-job-admin      分布式任务调度管理模块
8.   |   -ming-job-core       分布式任务调度核心依赖模块
9.  ming-modules 业务模块
10.  |   -ming-system-service       系统模块
11.  |       -ming-system-client      系统模块客户端层
12.  |       -ming-system-web         系统模块服务层
13.  |   -ming-user-service         用户模块
14.  |       -ming-user-client        用户模块客户端层
15.  |       -ming-user-web           用户模块服务层
16.  ming-rocketmq    消息队列模块


#### 项目备注
1. 纯个人从0设计搭建的微服务高并发项目,致力于研究学习.如有不同想法,欢迎共同探讨交流进步！
2. 微信联系方式:zhiruyahaha
