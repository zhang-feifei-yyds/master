package com.ming.common.design.singleton;

/**
 * @Classname Mgr01
 * @Description 饿汉式 就实例化一个单例 JVM保证线程安全
 * 简单实用  不推荐使用
 * 唯一缺点：不管是否用到  类装载时 就完成实例化
 * @Date 2021/2/22 14:13
 * @Created by yanming.fu
 */
public class Mgr01 {

    private static final  Mgr01 INSTANCE = new Mgr01();

    private Mgr01(){};

    public static Mgr01 getInstance(){
        return  INSTANCE;
    }


    public static void main(String[] args) {
        Mgr01 m1 = Mgr01.getInstance();
        Mgr01 m2 = Mgr01.getInstance();
        System.out.println(m1==m2);
    }


}
