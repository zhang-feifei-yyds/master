package com.ming.common.design.singleton;

/**
 * 静态内部类方式  最完美
 * JVM保证了单例  因为在加载外部类时不会加载内部类 可以实现懒加载
 * @Date 2021/2/22 14:17
 * @Created by yanming.fu
 */
public class Mgr07 {

    private Mgr07(){};

    private static class Mgr01Holder{
        private final static Mgr07 INSTANCE =new Mgr07();
    }


    public static  Mgr07 getInstance(){
        return Mgr01Holder.INSTANCE;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() ->{
                System.out.println(Mgr07.getInstance().hashCode());
            }).start();
        }
    }

}
