package com.ming.system.controller;

import com.ming.common.entity.ResultData;
import com.ming.system.mapper.SystemMapper;
import com.ming.system.service.SystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ming.system.System;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Classname UserController
 * @Description TODO
 * @Date 2021/1/5 13:57
 * @Created by yanming.fu
 */
@RestController
@Slf4j
@RequestMapping("/system")
@Api(value = "系统模块",tags={"系统模块"})
public class SystemController {

    @Autowired
    private SystemService systemsService;

    @Resource
    private SystemMapper systemsMapper;

    @GetMapping("/")
    @ApiOperation("/方法")
    public String main(){
        List<System> systems = systemsService.findSystems();
        return  systems.toString();
    }

    @GetMapping("/index")
    @ApiOperation("测试index方法")
    public String index(){
        return  "Hello system";
    }


    @ApiOperation("feign客户端echo方法")
    @RequestMapping(value = "/echo/{id}", method = RequestMethod.GET)
    public ResultData feign(@PathVariable("id")String id){
        //数据库插入数据
        int rows = systemsService.insert();

        if(rows>0){
            log.info("数据插入成功~");
        }
        Example example = new Example(System.class);
        example.createCriteria().andEqualTo("id",id);
        List<System> systems = systemsMapper.selectByExample(example);
        return ResultData.success(systems);
    }
}
