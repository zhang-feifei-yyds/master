package com.ming.user.constant;

/**
 * @Classname BasicAuthConstant
 * @Description Basic认证的常量
 * @Date 2021/2/3 16:17
 * @Created by yanming.fu
 */
public class BasicAuthConstant {

    public static final String APP_KEY="ming-auth-api";//企业KEY

    public static final String SECRET_KEY="asdf";  //企业密钥

    //内置得token
    public static final String INSIDE_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhbGwiXSwiZXhwIjoxNjA3MTYzNzgzLCJqdGkiOiI1Nzk0ZTY0MC01NDMxLTRkNDItOWZhNi04MWFkN2M4ODgzNjgiLCJjbGllbnRfaWQiOiJpbnNpZGUtYXBwIn0.KMm8fDnvXJ0o8RaCGeH8cG_LtE6Tt-hUVNGhlMbZEPBJjGnWUFc_2fzgLa51TAxpqYpswtT8_Dn8owakoAz6gAUC8XRl0sEteDW5_KZdFGxmWMObsL4EKOZtkpX1s-nJad6m_v4jLmc6RKipaqzXO_jC3yyPH5UvRle82-fkT7lLXsNfKLxfvrwjSpk-HlJMzCIi3KXJvqQYbakU-AukjpNuaC0-aG6Cvbg9t3PfMMzzKE4Q8hpSRN5yu_ySr1m246F-lRx9OCyhT20CQBX0lKRGnnM-GEka4dzw52hyZHvopTetdyNyGGkoWTi0X1GCirmS40gvQY-OjyovUhGWLA" ;
}
