package com.ming.user.mapper;

import com.ming.user.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Classname UserDao
 * @Description TODO
 * @Date 2021/1/5 13:59
 * @Created by yanming.fu
 */
public interface UserMapper extends Mapper<User> {



}
