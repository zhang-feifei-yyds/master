package com.ming.user;

import com.ming.user.UserApplication;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;


/**
 * @Classname JasyptTest
 * @Description TODO
 * @Date 2021/1/20 17:34
 * @Created by yanming.fu
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JasyptTest {

    @Resource
    private StringEncryptor stringEncryptor;

    @Autowired
    private WebApplicationContext wac ;

    private MockMvc mockMvc;


    //在测试之前注册mockmvc
    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }


    @Test
    public void testFinalEngine()  throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/user/user/fegin")//发送get请求
                .contentType(MediaType.APPLICATION_JSON_UTF8))//设置返回类型
                .andExpect(MockMvcResultMatchers.status().isOk())//添加期望，如果返回结果是ok的
                //用jsonpath来判断，如果返回的json是个集合且长度为3
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }


    @Test
    public void encryptPwd(){
        //加密
        String root = stringEncryptor.encrypt("182.92.96.8:8848");
        System.out.println("加密username: "+ root);
        String decrypt = stringEncryptor.decrypt(root);
        System.out.println("解密username: "+ decrypt);

        String password = stringEncryptor.encrypt("123456");
        System.out.println("加密password: "+ password);
        String decrypts = stringEncryptor.decrypt(password);
        System.out.println("解密username: "+ decrypts);

    }
}
