package com.ming.mq.common.config;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Classname MqImportSelector
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
public class MqImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.ming.mq.common.config.MingMqConfig",
       "com.ming.mq.common.config.TestConfig"
        };
    }
}
