package com.ming.mq.common.constant;

/**
 * @Classname TagConstant
 * @Description TODO
 * @Date 2021/1/13 11:32
 * @Created by yanming.fu
 */
public class TagConstant {

    /**
     * 对接用户模块
     */
    public static final String USER_TAG_NAME = "ming-user-tag";
    /**
     * 对接系统模块
     */
    public static final String SYSTEMTAG_NAME = "ming-system-tag";

}
