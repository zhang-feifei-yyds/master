package com.ming.mq.common.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @Classname JxStudySource
 * @Description 可通过callSuper=true解决(默认仅使用该类中定义的属性且不调用父类的方法)。让其生成的equals方法和hashcode方法包含父类属性
 * @Date 2021/1/13 14:59
 * @Created by yanming.fu
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class JxStudySource  implements Serializable {
    /**
     * 用户id
     */
    private  Long userId;

    /**
     * 班级id
     */
    private Integer classId;

    /**
     *试卷id
     */
    private Integer paperId;

}
