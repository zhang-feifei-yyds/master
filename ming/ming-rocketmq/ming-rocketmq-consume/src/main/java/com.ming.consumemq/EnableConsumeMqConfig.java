package com.ming.consumemq;

import com.ming.consumemq.config.ImportSelectorConsume;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Classname EnableConsumeMqConfig
 * @Description TODO
 * @Date 2021/1/13 11:01
 * @Created by yanming.fu
 */
@Configuration
@Import(ImportSelectorConsume.class)
public class EnableConsumeMqConfig {

}
