package com.ming.producemq.config;


import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Classname ProduceImportSelector
 * @Description TODO
 * @Date 2021/1/13 16:18
 * @Created by yanming.fu
 */

public class ProduceImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{
                "com.ming.producemq.config.UserMqConfig",
                "com.ming.producemq.config.SystemMqConfig"
        };
    }
}
